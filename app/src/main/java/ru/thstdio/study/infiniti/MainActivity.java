package ru.thstdio.study.infiniti;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    public PublishProcessor<Integer> subject;
    TextView text1, text2;
    int number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        text1 = findViewById(R.id.text_1);
        text2 = findViewById(R.id.text_2);
        initFlow();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(v -> fabClick());
    }

    private void initFlow() {
        subject = PublishProcessor.create();
  //      Flowable<Integer> out = subject.onBackpressureLatest();
        Flowable<Integer> out = subject.onBackpressureBuffer(1,
                () -> Toast.makeText(this, "OverFlow", Toast.LENGTH_SHORT),
                BackpressureOverflowStrategy.DROP_OLDEST);
        out.observeOn(Schedulers.io(),false,1).
                map(n -> longWork(n)).
                observeOn(AndroidSchedulers.mainThread()).
                subscribe(n -> text2.append("------------\n"));

    }

    private void fabClick() {
        number++;
        text1.append("click  " + number + "\n");
        subject.onNext(number);
    }

    private int longWork(int count) {
        try {
            ThreadUtil.runOnUiThread(() -> text2.append(String.format("Start %d\n", count)));
            Thread.sleep(2000);
            ThreadUtil.runOnUiThread(() -> text2.append(String.format("Finish %d\n", count)));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return count;
    }

    private void printTxt2(String format) {
        text2.setText(format);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
